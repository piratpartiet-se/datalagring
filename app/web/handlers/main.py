from app.web.handlers.base import BaseHandler
from datetime import date

from app.logger import logger


class MainHandler(BaseHandler):
    async def get(self):
        d0 = date(2019, 10, 16)
        d1 = date.today()
        day = d0 - d1

        logger.debug("Date delta calculated to: " + day.__str__())

        await self.render("index.html",
                          day=day.days)
