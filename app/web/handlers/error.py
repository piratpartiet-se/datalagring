import tornado.web

from app.web.handlers.base import BaseHandler


class Error404Handler(BaseHandler):
    async def prepare(self):
        """
        Do not call manually. This runs on every request before get/post/etc.
        """
        raise tornado.web.HTTPError(404)
